# PRJ303 (Visualization and forecasting of covid 19)



## Overview of our project
This system will enable the users to visualize the covid-19 situation globally and locally in terms of confirmed cases, death cases, and vacinnation coverage around the world. It will also be used to forecast future possible positive cases and death cases based on prior observations.
 


## Aim
To do a global forecasting of new cases and new deaths rate for covid-19 and to do a data visualization of the covid-19 situation globally in order to keep the people informed about covid-19 cases and its looming threat



## Objectives

To visualize the impact of the coronavirus globally and in Bhutan.
To analyze the vaccination coverage of covid-19 and relate it with the death cases.
To analyze and visualize which age group of people got more covid-19 virus.
To do a time series forecasting of new cases and new deaths rate for covid-19.

## Algorithms
SARIMA (Seasonal Autoregressive Integrated Moving Average)
The SARIMA model was used to predict the future confirmed cases and death cases COVID-19. 


## Model Evaluation
To compare the performance of models quantitatively, we have calculated the value of ACI(Akaike Information Criterion).
The Akaike Information Criterion (AIC) is an estimator of prediction error and thereby relative quality of statistical models for a given set of data. AIC will test how well the model fits the dataset without over-fitting it. The AIC score rewards models that achieve a high goodness-of-fit score and penalizes them if they become overly complex. 

### Deployment

## Website Link : 
http://covid-forcasting.herokuapp.com/


## promotional video link:
https://drive.google.com/file/d/1kYloJdJUQVvvNfqzeje5uvAhYn_MioDF/view?usp=sharing
